{-# language QuasiQuotes #-}

-- | CSS style as a raw string

module Web.Sharelinks.Style where

import Text.RawString.QQ
import qualified Data.Text.Lazy as TL


-- CSS --

style :: TL.Text
style = TL.pack [r|
html {
  min-width: 500px;
}

body {
  margin-top: 40px;
  color: #CCC;
  background-color: #333;
  font-family: monospace;
  font-size: 2em;
}

a {
  color: pink;
}

a:hover {
  color: #fb7a91;
}

.main {
  box-sizing: border-box;
  margin: auto;
}

.add-div {
  min-width: 400px;
  max-width: 90%;
  margin: 20px auto;
}

ul {
  list-style-type: none;
  padding-left: 0;
  margin: 0;
}

li {
  padding-bottom: 0.7em;
  list-style-position:inside;
  overflow: hidden;
  text-overflow: "...";
}

input[type=text], input[type=url] {
  width: 100%;
}
input[type=submit] {
  width: 6em;
}
input {
  font-size: 1em;
  height: 1.5em;
}

input::placeholder, textarea::placeholder {
  font-size: 1em;
}

textarea {
  width: 100%;
  box-sizing: border-box;
  font-size: 1em;
}

.header-h1 {
  display: inline-block;
  margin: 20px 20px;
  margin-right: 0;
}
.header- {
  display: inline-block;
  margin: 1em;
}
.header {
  color: #86c6ff;
  text-decoration: none;
}
.header:hover {
  color: #4aaaff;
}

h4 {
  color: #fdfd95;
  margin: 0;
}

.links {
  min-width: 400px;
  max-width: 98%;
  margin: 20px auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.link, .link-list {
  display: inline-block;
  margin-right: 0.5em;
  margin-bottom: 0.5em;
  padding: 0.8em;
  border: #666 1px dashed;
  flex-grow: 1;
  # max-width: 45%;
  min-width: 20em;
}

.link-list {
  width: 100%;
}

.new {
  display: inline-block;
}

.newlink {
  font-size: 1.1em;
  font-weight: bold;
  color: #caf5ad;
  text-align:center;
  display: inline-block;
}

.newlink:hover {
  color: #7ef72c;
}

.delete {
  float: right;
  clear: both;
}

.deletebtn {
  background-color: #f44336;
  border: none;
  color: white;
  padding-left: 0.3em;
  padding-right: 0.3em;
  padding-top: 0.1em;
  padding-bottom: 0.1em;
  text-align: center;
  font-weight: bold;
  text-decoration: none;
  display: inline-block;
  font-family: monospace;
  font-size: 1.1em;
  width: 1.5em !important;
  height: 1.5em;
}
.deletebtn:hover {
  background-color: #b00;
}

.date {
  font-size: 0.7em;
  margin-right: 0.5em;
}

.tag {
  color: #c0f3ff;
  text-decoration: none;
  font-size: 0.7em;
}

.tag:hover {
  color: #73dff9;
  text-decoration: underline;
  font-size: 0.7em;
}

.comma {
  margin-right: 0.5em;
  font-size: 0.7em;
}

.tags {
  max-width: 98%;
  margin: 20px auto;
  text-align: center;
}
|]
