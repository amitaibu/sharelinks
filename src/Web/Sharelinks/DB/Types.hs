-- | Defines the link data type and how to store/read it.
--   Currently just using a simple json file.

module Web.Sharelinks.DB.Types where

import Data.Aeson.Types (prependFailure, typeMismatch)
import Data.Aeson
import Data.Maybe
import Data.Time
import qualified Data.Text as T
import Control.Concurrent.STM

data Message
  = AddLink Link
  | RemoveLink T.Text
  | GetLinks (TMVar [Link])

data Link
  = Link
  { _title :: T.Text
  , _date :: Day
  , _desc :: T.Text
  , _link :: T.Text
  , _tags :: [T.Text]
  }
  deriving (Show)

instance FromJSON Link where
  parseJSON = \case
    Object o -> do
      Link
        <$> o .: "title"
        <*> o .: "date"
        <*> o .: "desc"
        <*> o .: "link"
        <*> fmap (fromMaybe []) (o .:? "tags")
    invalid -> prependFailure
      "parsing Link failed, "
      (typeMismatch "Object" invalid)

instance ToJSON Link where
  toJSON Link{..} = object
    [ "title" .= _title
    , "date" .= _date
    , "desc" .= _desc
    , "link" .= _link
    , "tags" .= _tags
    ]


