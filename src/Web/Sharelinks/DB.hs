-- | API for the DB
--   Currently just using a simple json file.

module Web.Sharelinks.DB
  ( module Export
  )
where

import Web.Sharelinks.DB.Types as Export
import Web.Sharelinks.DB.Internal as Export
  ( initializeDB
  , defaultDbFilePath
  , parseLink
  , addLink
  , removeLink
  , getLinks
  , toMD5
  , dbWorker
  )

