-- | Configuration

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}  -- One more extension.
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}  -- To derive Show
{-# LANGUAGE TypeOperators      #-}

module Web.Sharelinks.Config
  ( Config(..)
  , DisplayMode(..)
  , FilterMode(..)
  , parseArgs
  )
where

import Data.Maybe (fromMaybe)
import Data.SecureMem
import Data.Text.Encoding (encodeUtf8)
import Options.Generic

data Config
  = Config
  { cfgPort :: Int
  , cfgName :: Text
  , cfgJson :: FilePath
  , cfgDisplayMode :: DisplayMode
  , cfgFilterMode :: FilterMode
  , cfgUsername :: SecureMem
  , cfgPassword :: Maybe SecureMem
  }

data DisplayMode
  = Grid
  | List

data FilterMode
  = Accumulate
  | Replace

data Opts w
  = Opts
  { port :: Maybe Int <?> "PORT"
  , name :: Maybe Text <?> "The name of the website"
  , json :: Maybe FilePath <?> "PATH to database json file"
  , list :: Bool <?> "Display entries as a list instead of a grid"
  , accumulateTags :: Bool <?> "Filter by accumulating tags"
  , username :: Maybe Text <?> "basic authorization username - default is admin"
  , password :: Maybe Text <?> "basic authorization password - default is none"
  }
  deriving (Generic)

instance ParseRecord (Opts Wrapped)
deriving instance Show (Opts Unwrapped)

parseArgs :: FilePath -> IO Config
parseArgs defaultJson = do
  cfg <- unwrapRecord "sharelinks"
  pure $ Config
    { cfgPort = fromMaybe 3000 (unHelpful $ port cfg)
    , cfgName = fromMaybe "Links" (unHelpful $ name cfg)
    , cfgJson = fromMaybe defaultJson (unHelpful $ json cfg)
    , cfgFilterMode = if unHelpful $ accumulateTags cfg then Accumulate else Replace
    , cfgDisplayMode = if unHelpful $ list cfg then List else Grid
    , cfgUsername =
      ( fromMaybe (secureMemFromByteString "admin")
      . fmap (secureMemFromByteString . encodeUtf8)
      . unHelpful
      . username
      ) cfg
    , cfgPassword =
      fmap
        (secureMemFromByteString . encodeUtf8)
        (unHelpful $ password cfg)
    }
