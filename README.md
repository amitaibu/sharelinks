# Minimalistic website for link sharing

Get the latest Linux binary here: [![Build status](https://gitlab.com/gilmi/sharelinks/badges/master/pipeline.svg)](https://gitlab.com/gilmi/sharelinks/-/jobs/artifacts/master/browse?job=build)

![Example](example.png)

This website will let anyone with access to it to add and delete links.
It can also be configured to use http basic authorization using the `--username` and `--password` flags.

It uses a single json file as the database.
By default it wll write to `~/.local/sharelinks/db.json` but this is configurable with the `--json` flag.

## Build & Run

Prerequisites: [Stack](https://haskellstack.org) (Could probably also be built with [Cabal+GHC](https://www.haskell.org/downloads/) instead).

```sh
stack build && stack exec -- sharelinks-server
```

Will run the website on port 3000 and without http basic auth.

---

For additional configuration options run `--help`.
